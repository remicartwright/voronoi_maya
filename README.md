# Voronoi_Maya
The goal is to recreate diagrams such as the one below.

![A Voronoi diagram](https://leatherbee.org/wp-content/uploads/2018/10/100plates3relax.png)

This project contains script to generate a Voronoi diagram from given or random points in AUTODESK MAYA.

## How to use
- Open your maya version.
- Open the script editor. Windows > General Editors > Script Editor
- Create a new tab in the script editor
- File > Open the two scripts in two separate tabs.
- Change line 25 in Vonoroi_2D_generator.mel
```
source "path/Voronoi/BowyerWatsonTriangulation.mel";
```
